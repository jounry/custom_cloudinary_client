library custom_cloudinary_client;

import 'data/ImageClient.dart';
import 'data/VideoClient.dart';
import 'models/CloudinaryResponse.dart';

class CloudinaryClient {
  String _apiKey;
  String _apiSecret;
  String _cloudName;
  ImageClient _imageClient;
  VideoClient _videoClient;

  CloudinaryClient(String apiKey, String apiSecret, String cloudName) {
    this._apiKey = apiKey;
    this._apiSecret = apiSecret;
    this._cloudName = cloudName;
    _imageClient = ImageClient(_apiKey, _apiSecret, _cloudName);
    _videoClient = VideoClient(_apiKey, _apiSecret, _cloudName);
  }

  void uploadImage(String imagePath,
      {String filename,
      String folder,
      String publicId,
      OnImageUploadingProgressCallBack onImageUploadingProgressCallBack,
      OnImageUploadSuccessCallBack onImageUploadSuccessCallBack,
      OnImageUploadFailCallBack onImageUploadFailCallBack}) async {
    _imageClient.uploadImage(imagePath,
        imageFilename: filename,
        publicId: publicId,
        folder: folder,
        onImageUploadFailCallBack: onImageUploadFailCallBack,
        onImageUploadSuccessCallBack: onImageUploadSuccessCallBack,
        onImageUploadingProgressCallBack: onImageUploadingProgressCallBack);
  }

  void deleteImage(String publicId,
      {OnDeleteSuccessCallBack onDeleteSuccessCallBack,
      OnDeleteFailCallBack onDeleteFailCallBack}) {
    _imageClient.deleteImage(
        publicId, onDeleteSuccessCallBack, onDeleteFailCallBack);
  }

//  Future<List<CloudinaryResponse>> uploadImages(
//    List<String> imagePaths, {
//    String filename,
//    String folder,
//  }) async {
//    List<CloudinaryResponse> responses = List();
//
//    for (var path in imagePaths) {
//      CloudinaryResponse resp = await _imageClient.uploadImage(
//        path,
//        imageFilename: filename,
//        folder: folder,
//      );
//      responses.add(resp);
//    }
//    return responses;
//  }
//
//  Future<List<String>> uploadImagesStringResp(List<String> imagePaths,
//      {String filename, String folder}) async {
//    List<String> responses = List();
//
//    for (var path in imagePaths) {
//      CloudinaryResponse resp = await _imageClient.uploadImage(path,
//          imageFilename: filename, folder: folder);
//      responses.add(resp.url);
//    }
//    return responses;
//  }

  Future<CloudinaryResponse> uploadVideo(String videoPath,
      {String filename, String folder}) async {
    return _videoClient.uploadVideo(videoPath,
        videoFileName: filename, folder: folder);
  }
}
