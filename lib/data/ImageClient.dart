import 'dart:convert';

import 'package:crypto/crypto.dart';
import 'package:custom_cloudinary_client/models/CloudinaryResponse.dart';
import 'package:dio/dio.dart';

import 'BaseApi.dart';

typedef OnImageUploadingProgressCallBack = void Function(double percent);

typedef OnImageUploadSuccessCallBack = void Function(
    CloudinaryResponse response);

typedef OnImageUploadFailCallBack = void Function(dynamic error);

typedef OnDeleteSuccessCallBack = void Function();

typedef OnDeleteFailCallBack = void Function(dynamic error);

class ImageClient extends BaseApi {
  String _cloudName;
  String _apiKey;
  String _apiSecret;

  ImageClient(String apiKey, String apiSecret, String cloudName) {
    this._apiKey = apiKey;
    this._apiSecret = apiSecret;
    this._cloudName = cloudName;
  }

  void uploadImage(String imagePath,
      {String imageFilename,
      String publicId,
      String folder,
      OnImageUploadingProgressCallBack onImageUploadingProgressCallBack,
      OnImageUploadSuccessCallBack onImageUploadSuccessCallBack,
      OnImageUploadFailCallBack onImageUploadFailCallBack}) async {
    int timeStamp = new DateTime.now().millisecondsSinceEpoch;

    Map<String, dynamic> params = new Map();
    if (_apiSecret == null || _apiKey == null) {
      throw Exception("apiKey and apiSecret must not be null");
    }

    params["api_key"] = _apiKey;

    if (imagePath == null) {
      throw Exception("imagePath must not be null");
    }
    if (publicId == null) {
      publicId = imagePath.split('/').last;
      publicId = publicId.split('.')[0];
    }

    if (imageFilename != null) {
      publicId = imageFilename.split('.')[0] + "_" + timeStamp.toString();
    } else {
      imageFilename = publicId;
    }

    if (folder != null) {
      params["folder"] = folder;
    }

    if (publicId != null) {
      params["public_id"] = publicId;
    }

    params["file"] =
        await MultipartFile.fromFile(imagePath, filename: imageFilename);
    params["timestamp"] = timeStamp;
    params["signature"] = getSignature(folder, publicId, timeStamp);

    FormData formData = new FormData.fromMap(params);

    Dio dio = await getApiClient();
    double progressPercent = 0;
    try {
      Response response = await dio.post(_cloudName + "/image/upload",
          data: formData, onSendProgress: (int count, int total) {
        progressPercent += ((count / total) * 100) / 2;
        onImageUploadingProgressCallBack(progressPercent);
      }, onReceiveProgress: (int count, int total) {
        progressPercent += ((count / total) * 100) / 2;
        onImageUploadingProgressCallBack(progressPercent);
      });
      onImageUploadSuccessCallBack(
          CloudinaryResponse.fromJsonMap(response.data));
    } catch (error, stacktrace) {
      print("Exception occurred: $error stackTrace: $stacktrace");
      onImageUploadFailCallBack(error);
    }
  }

  void deleteImage(
    String publicId,
    OnDeleteSuccessCallBack onDeleteSuccessCallBack,
    OnDeleteFailCallBack onDeleteFailCallBack,
  ) async {
    if (publicId == null) {
      throw Exception("publicId must not be null");
    }

    Dio dio = await getApiClient();

    Map<String, dynamic> params = new Map();
    Map<String, dynamic> headers = new Map();
    String basicAuth =
        'Basic ' + base64Encode(utf8.encode('$_apiKey:$_apiSecret'));
    headers['authorization'] = basicAuth;

    params["public_ids"] = [publicId];

    FormData formData = new FormData.fromMap(params);

    if (_apiSecret == null || _apiKey == null) {
      throw Exception("apiKey and apiSecret must not be null");
    }
    try {
      Response response = await dio.delete(
          _cloudName + "/resources/image/upload",
          data: formData,
          options: Options(headers: headers));
      onDeleteSuccessCallBack();
    } catch (error, stacktrace) {
      print("Exception occurred: $error stackTrace: $stacktrace");
      onDeleteFailCallBack(error);
    }
  }

  String getSignature(String folder, String publicId, int timeStamp) {
    var buffer = new StringBuffer();
    if (folder != null) {
      buffer.write("folder=" + folder + "&");
    }
    if (publicId != null) {
      buffer.write("public_id=" + publicId + "&");
    }
    buffer.write("timestamp=" + timeStamp.toString() + _apiSecret);

    var bytes = utf8.encode(buffer.toString().trim()); // data being hashed

    return sha1.convert(bytes).toString();
  }
}
